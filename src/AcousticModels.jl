module AcousticModels

include("libModels.jl")

export decibel
export spherical_impedance
export spherical_wave
export angular_wave_vector_component
export angular_wave_vector
export mode_frequency
export mode
export mesh_grid
export index_grid
export room_axes
export nominal_third_octave_frequencies
export exact_third_octave_bands

end
