using LinearAlgebra

"""
    decibel(x, ref)

Return the value of x converted to dB with respect reference ref.
# Example
```jldoctest
julia> decibel(0.1, 20e-6)
73.97940008672037

julia> decibel.(rand(10), 20e-6)
10-element Array{Float64,1}:
 90.4303371172515
 87.76785692543072
 92.21939475560748
 92.67736239261843
 92.08466790845944
 89.16516336782827
 92.61746822932335
 60.04807642155754
 93.31368974737545
 77.7385164215302
```
"""
function decibel(x::T, ref::Real) where {T<:Number}
    return 20.0 * log10(abs(x) / ref)
end

"""
    spherical_impedance(f, r, ρ, c)

Compute the complex impedance for a sphere wave of frequency f Hz, at distance r m from the radiation centre, in a medium with density ρ kg/m^3 and phase speed of sound c m/s.
# Example
```jldoctest
julia> spherical_impedance(1000, 1, 1.205, 343)
412.08694629151245 + 22.49588634867694im

julia> spherical_impedance.(1000:1010, 1, 1.205, 343)
11-element Array{Complex{Float64},1}:
 412.08694629151245 + 22.49588634867694im
  412.0893914474561 + 22.47354628381181im
 412.09182931511435 + 22.45125041282011im
 412.09425992339555 + 22.42899860509016im
  412.0966833010649 + 22.406790730522857im
 412.09909947674544 + 22.38462665952921im
 412.10150847891873 + 22.362506263027793im
   412.103910335926 + 22.34042941244232im
  412.1063050759685 + 22.318395979699126im
 412.10869272710914 + 22.29640583722482im
 412.11107331727226 + 22.274458857943745im
```
"""
function spherical_impedance(
    f::Real,
    r::Real,
    ρ::Real,
    c::Real
    )

    kr = 2π * f * r / c

    return im * ρ * c * kr / (1.0 + im * kr)

end

"""
    spherical_wave(U, a, f, r, t, r, ρ, c)

Compute the complex wave radiated by a pulsating sphere with surface velocity U m/s, radius a m, frequency f Hz; at time t s, at a distance from the radiation centre of r m; in a medium of density ρ kg/m^3 and phase speed of sound c m/s.

# Example
```jldoctest
julia> spherical_wave(0.75, 0.005, 1000, 0, 1, 1.205, 343)
-0.07164793924384269 + 0.12186780546373502im

julia> spherical_wave.(0.75, 0.005, 1000:1010, 0, 1, 1.205, 343)
11-element Array{Complex{Float64},1}:
  -0.07164793924384269 + 0.12186780546373502im
  -0.06947254870990466 + 0.12328183644151193im
  -0.06726937288685036 + 0.12465702154789704im
  -0.06503910573607367 + 0.1259928169183268im
  -0.06278245182783268 + 0.1272886910914245im
 -0.060500126119449237 + 0.12854412521530148im
  -0.05819285372950454 + 0.1297586132498912im
 -0.055861369708107296 + 0.130931662165236im
  -0.05350641880330684 + 0.1320627921356506im
  -0.05112875522372149 + 0.1331515367296897im
  -0.04872914239745705 + 0.13419744309584594im
```
"""
function spherical_wave(
    U::Real,
    a::Real,
    f::Real,
    t::Real,
    r::Real,
    ρ::Real,
    c::Real
    )

    Z = spherical_impedance(f, a, ρ, c)

    ω = 2π * f
    k = ω / c

    ϕ = mod2pi(ω * t - k * (r - a))

    return Z * a * U * exp(im * ϕ) / r

end

"""
    angular_wave_vector_component(n, L)

Returns the angular wave vector component for a rectangular room given the
mode index and room length along the required component cartesian direction, in
meters.
# Example
```jldoctest
julia> nx = 7
7

julia> Lx = 5
5

julia> kx = angular_wave_vector_component(nx, Lx)
4.39822971502571
```
"""
function angular_wave_vector_component(n::Integer, L::Real)
    return n * π / L
end

"""
    angular_wave_vector(nx, ny, nz, Lx, Ly, Lz)

Returns the angular wave vector of a rectangular room mode. Mode numbers along
the cartesian directions are nx ny and nz, the room sizes along the cartesian
directions are Lx, Ly and Lz meters.
# Example
```jldoctest
julia> angular_wave_vector(1, 4, 7, 5.0, 4.0, 3.0)
3-element Array{Float64,1}:
 0.6283185307179586
 3.141592653589793
 7.330382858376184
```
"""
function angular_wave_vector(
    nx::Integer, 
    ny::Integer, 
    nz::Integer,
    Lx::Real, 
    Ly::Real, 
    Lz::Real
    )

    return [
        angular_wave_vector_component(nx, Lx);
        angular_wave_vector_component(ny, Ly);
        angular_wave_vector_component(nz, Lz)
        ]

end

"""
    mode_frequency(nx, ny, nz, Lx, Ly, Lz, c)

Returns the modal frequency of a mode characterized by indeces nx, ny and nz
along the cartesian directions for a rectangular room of sizes Lx, Ly and Lz
meters along the cartesian directions. c is the speed of sound in air, by
default 343 meters per second (speed at standard conditions).
# Example
```jldoctest
julia> mode_frequency(1, 4, 7, 5.0, 4.0, 3.0)
436.7174156260672

julia> mode_frequency(1, 4, 7, 5.0, 4.0, 3.0, 340.0)
432.89772977511035
```
"""
function mode_frequency(
    nx::Integer, 
    ny::Integer, 
    nz::Integer,
    Lx::Real, 
    Ly::Real, 
    Lz::Real,
    c::Real = 343
    )

    return norm(angular_wave_vector(nx, ny, nz, Lx, Ly, Lz)) * c / 2π

end

"""
    mode(x, y, z, nx, ny, nz, Lx, Ly, Lz)

Returns the modal shape at coordinates in x, y, z for mode characterized by
indeces nx, ny and nz along the cartesian directions for a rectangular room of
sizes Lx, Ly and Lz meters along the cartesian directions. Result returned
normalized between 1 and -1, arbitrary units.
# Example
```jldoctest
julia> mode(2.5, 2.0, 1.5, 1, 4, 7, 5.0, 4.0, 3.0)
0.0

julia> mode.([3.0; 2.5], [1.0; 2.0], [0.75; 1.5], 1, 4, 7, 5.0, 4.0, 3.0)
2-element Array{Float64,1}:
 0.2185080122244105
 0.0
```
"""
function mode(
    x::Real, 
    y::Real, 
    z::Real,
    nx::Integer, 
    ny::Integer, 
    nz::Integer,
    Lx::Real, 
    Ly::Real, 
    Lz::Real
    )

return cospi(nx * x / Lx) * cospi(ny * y / Ly) * cospi(nz * z / Lz)

end

"""
    mesh_grid(x, y, z)

Returns the 3-D grid coordinates defined by the vectors x, y, and z.
The grid is represented by three matrices of size length(y) by length(x) by
length(z). This is similar to MATLAB's meshgrid.
# Example
```jldoctest
julia> x = [1; 2]
2-element Array{Int64,1}:
 1
 2

julia> y = [3; 4]
2-element Array{Int64,1}:
 3
 4

julia> z = [5; 6]
2-element Array{Int64,1}:
 5
 6

julia> X, Y, Z = mesh_grid(x, y, z)
([1 2; 1 2]

[1 2; 1 2], [3 3; 4 4]

[3 3; 4 4], [5 5; 5 5]

[6 6; 6 6])
```
"""
function mesh_grid(x::AbstractArray, y::AbstractArray, z::AbstractArray)

    X = zeros(eltype(x), length(y), length(x), length(z))
    fX(i, j, k) = X[i, j, k] = x[j]
    [fX(i, j, k) for i in 1:length(y), j in 1:length(x), k in 1:length(z)]

    Y = zeros(eltype(y), length(y), length(x), length(z))
    fY(i, j, k) = Y[i, j, k] = y[i]
    [fY(i, j, k) for i in 1:length(y), j in 1:length(x), k in 1:length(z)]

    Z = zeros(eltype(z), length(y), length(x), length(z))
    fZ(i, j, k) = Z[i, j, k] = z[k]
    [fZ(i, j, k) for i in 1:length(y), j in 1:length(x), k in 1:length(z)]

    return X, Y, Z

end

"""
    index_grid(Nx, Ny, Nz)

Returns a 3-D grid up to indeces Nx, Ny and Nz along the 3 cartesian
directions. The grid is represented by three matrices of size Ny by Nx by Nz.
# Example
```jldoctest
julia> Gx, Gy, Gz = index_grid(3, 4, 5)
([1 2 3; 1 2 3; 1 2 3; 1 2 3]

[1 2 3; 1 2 3; 1 2 3; 1 2 3]

[1 2 3; 1 2 3; 1 2 3; 1 2 3]

[1 2 3; 1 2 3; 1 2 3; 1 2 3]

[1 2 3; 1 2 3; 1 2 3; 1 2 3], [1 1 1; 2 2 2; 3 3 3; 4 4 4]

[1 1 1; 2 2 2; 3 3 3; 4 4 4]

[1 1 1; 2 2 2; 3 3 3; 4 4 4]

[1 1 1; 2 2 2; 3 3 3; 4 4 4]

[1 1 1; 2 2 2; 3 3 3; 4 4 4], [1 1 1; 1 1 1; 1 1 1; 1 1 1]

[2 2 2; 2 2 2; 2 2 2; 2 2 2]

[3 3 3; 3 3 3; 3 3 3; 3 3 3]

[4 4 4; 4 4 4; 4 4 4; 4 4 4]

[5 5 5; 5 5 5; 5 5 5; 5 5 5])

```
"""
function index_grid(Nx::Integer, Ny::Integer, Nz::Integer)
    return mesh_grid(0:Nx, 0:Ny, 0:Nz)
end

"""
    room_axes(rx, ry, rz, Lx, Ly, Lz)

Returns the 1-D axes coordinates along the sides of a rectangular room with
reference to a corner. The room has sizes Lx, Ly and Lz meters along the
cartesian directions, while rx, ry and rz express the step of the axes along
each cartesian direction.
"""
function room_axes(
    rx::Real, 
    ry::Real, 
    rz::Real,
    Lx::Real, 
    Ly::Real, 
    Lz::Real
    )

    x = range(0.0, stop = Lx, length = round(Integer, 1 + Lx / rx))
    y = range(0.0, stop = Ly, length = round(Integer, 1 + Ly / ry))
    z = range(0.0, stop = Lz, length = round(Integer, 1 + Lz / rz))

    return x, y, z

end

"""
    room_grid(rx, ry, rz, Lx, Ly, Lz)

Returns the 3-D grid coordinates inside a rectangular room with reference to a
corner. The room has sizes Lx, Ly and Lz meters along the cartesian directions,
while rx, ry and rz express the step of the grid along each cartesian direction.
"""
function room_grid(
    rx::Real, 
    ry::Real, 
    rz::Real,
    Lx::Real, 
    Ly::Real, 
    Lz::Real
    )

    x, y, z = room_axes(rx, ry, rz, Lx, Ly, Lz)

    return mesh_grid(x, y, z)

end

"""
    nominal_third_octave_frequencies()

Returns an array of the nominal third octave centre frequencies.

# Example
```jldoctest

julia> f = nominal_third_octave_frequencies()
32-element Array{Float64,1}:
    16.0
    20.0
    25.0
    31.5
    40.0
    50.0
    63.0
    80.0
     ⋮
  5000.0
  6300.0
  8000.0
 10000.0
 12500.0
 16000.0
 20000.0

julia> f[(f .<=100) .& (f .>= 10)]
9-element Array{Float64,1}:
  16.0
  20.0
  25.0
  31.5
  40.0
  50.0
  63.0
  80.0
 100.0

```
"""
function nominal_third_octave_frequencies()

    return [
        16;
        20;
        25;
        31.5;
        40;
        50;
        63;
        80;
        100;
        125;
        160;
        200;
        250;
        315;
        400;
        500;
        630;
        800;
        1000;
        1250;
        1600;
        2000;
        2500;
        3150;
        4000;
        5000;
        6300;
        8000;
        10000;
        12500;
        16000;
        20000
    ]

end

"""
exact_third_octave_bands()

Returns tree arrays for the exact lower limits, centre frequencies and upper
limits of the third octave frequency bands.
"""
function exact_third_octave_bands()

    fcentre  = 10^3 .* (2 .^ ((-18:13) ./ 3))
    fd = 2^(1/6);
    fupper = fcentre .* fd
    flower = fcentre ./ fd

    return flower, fcentre, fupper

end

