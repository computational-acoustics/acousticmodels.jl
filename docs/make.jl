using AcousticModels
using Documenter

makedocs(;
    modules=[AcousticModels],
    authors="Stefano Tronci <tronci.stefano@gmail.com>",
    repo="https://gitlab.com/stefano-tronci/AcousticModels.jl/blob/{commit}{path}#L{line}",
    sitename="AcousticModels.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://stefano-tronci.gitlab.io/AcousticModels.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
