# AcousticModels

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://stefano-tronci.gitlab.io/AcousticModels.jl/dev)
[![Build Status](https://gitlab.com/stefano-tronci/AcousticModels.jl/badges/master/pipeline.svg)](https://gitlab.com/stefano-tronci/AcousticModels.jl/pipelines)
[![Coverage](https://gitlab.com/stefano-tronci/AcousticModels.jl/badges/master/coverage.svg)](https://gitlab.com/stefano-tronci/AcousticModels.jl/commits/master)

Julia package implementing simple acoustic models.

# Description

This package implements simple acoustics models for the simulation of of acoustic phenomena.

## Should I use this Package?

This package exists mainly for the author personal convenience and it will be developed on a as-needed basis. You are welcome to use this package, but you should perhaps not base your projects on it.

# Installation

In the Julia REPL, switch to package mode with `]` and issue:

```julia
(@v1.5) pkg> add https://gitlab.com/computational-acoustics/acousticmodels.jl.git
```

